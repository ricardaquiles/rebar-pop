import { Component, Input, OnChanges, ViewChild } from '@angular/core';

import * as roughjs from 'roughjs';

@Component({
  selector: 'app-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss']
})
export class ViewerComponent implements OnChanges {

  @Input() path = [];
  @ViewChild('canvas') canvas;
  SCALE_FACTOR = 0.5;
  POINTS_DIAMETER = 10;
  endPointsOptions = {
    roughness: 0,
    fill: '#666666',
    fillStyle: 'solid',
    stroke: '#666666',
    strokeWidth: 14
  };
  circleOptions = {
    roughness: 0,
    fill: '#666666',
    fillStyle: 'solid',
    stroke: '#666666',
    strokeWidth: 0
  };
  lineOptions = {
    roughness: 0,
    stroke: '#666666',
    strokeWidth: 14
  };

  constructor() { }

  ngOnChanges(changes) {
    if (changes['path'] && !changes['path'].firstChange) {
      this.drawShape();
    }
  }

  drawShape() {
    const ctx = this.canvas.nativeElement.getContext('2d');
    const rc = roughjs.canvas(this.canvas.nativeElement);

    // reset scale
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    // clear previous drawing
    ctx.clearRect(
      0, 0,
      // canvas.width * 1 / this.SCALE_FACTOR,
      // canvas.height * 1 / this.SCALE_FACTOR
      this.canvas.nativeElement.width,
      this.canvas.nativeElement.height
    );
    // apply the scale
    ctx.scale(this.SCALE_FACTOR, this.SCALE_FACTOR);

    // draw lines
    if (this.path.length > 1) {
      rc.linearPath(this.path, this.lineOptions);
    };
    // fill intersections
    this.path.forEach(([x, y]) => {
      rc.circle(x, y, this.POINTS_DIAMETER, this.circleOptions);
    });
    // draw points at start and end
    const [startCoordX, startCoordY] = this.path[0];
    rc.circle(startCoordX, startCoordY, this.POINTS_DIAMETER, this.endPointsOptions);
    const [endCoordX, endCoordY] = [...this.path].pop();
    rc.circle(endCoordX, endCoordY, this.POINTS_DIAMETER, this.endPointsOptions);
  }

}
