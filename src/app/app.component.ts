import {
  Component,
  AfterViewChecked
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewChecked {

  shapes = [];
  validations = 0;
  elapsedTimeValidating = 0;
  elapsedTimeGenerating = 0;
  startTimeRendering = null;
  numberToDownload;
  selectedShapeCoords = [];
  startingPoint = [150, 300]; // initial point where to start drawing
  tableHeight;
  settings = {
    stretchH: 'all',
    preventOverflow: 'vertical'
  };
  DIMAMETERS = [12, 10, 8, 6, 4, 3, 2, 1];
  RADIUS = [50, 40, 30, 25, 20, 15, 10, 8, 6, 4];
  REVS = ['A', 'B', 'C'];
  TYPES = [ '501', '308', '986', '702'];
  STEEL_SORTS = ['B 500 SD', 'C 256 SD', 'A 1500 SD'];
  FILENAMES = {
    200: 'shapes200',
    2000: 'shapes2k',
    10000: 'shapes10k'
  };

  constructor(private http: HttpClient) {
    // this.shapes.push(
    //   this.shapeObject(
    //     true, 123, 1, 89, 1, 12, '308', null,
    //     120, 600, 120, 240, null, null, null, undefined, null, undefined, undefined, null, null, null,
    //     'A 1500 SD', 4, 'B', '12d.'
    //   ),
    //   this.shapeObject(
    //     true, 1, 0, 89, 10, 3, '501', null,
    //     200, 100, 750, 350, 800, null, null, undefined, null, undefined, undefined, null, null, null,
    //     'B 500 SD', 5, 'C', '1w.'
    //   ),
    //   this.shapeObject(
    //     true, 33, 3, 100, 6, 1, '308', null,
    //     300, 150, 225, 135, null, null, null, undefined, null, undefined, undefined, null, null, null,
    //     'C 256 SD', 50, 'C', '2d.'
    //   ),
    //   this.shapeObject(
    //     true, 589, 2, 89, 8, 6, '308', null,
    //     110, 225, 30, 180, null, null, null, undefined, null, undefined, undefined, null, null, null,
    //     'B 500 SD', 5, 'A', '8d.'
    //   ),
    //   this.shapeObject(
    //     true, 356, 2, 388, 1, 2, '986', null,
    //     50, 155, 100, 150, null, null, null, undefined, null, undefined, undefined, null, null, null,
    //     'B 500 SD', 5, 'C', '10d.'
    //   ),
    //   this.shapeObject(
    //     true, 2, 0, 89, 10, 3, '986', null,
    //     300, 50, 100, 80, null, null, null, undefined, null, undefined, undefined, null, null, null,
    //     'B 500 SD', 5, 'C', '1w.'
    //   ),
    //   this.shapeObject(
    //     true, 2, 0, 89, 10, 3, '501', null,
    //     150, 75, 400, 150, 50, null, null, undefined, null, undefined, undefined, null, null, null,
    //     'B 500 SD', 5, 'C', '1w.'
    //   ),
    //   this.shapeObject(
    //     true, 2, 0, 89, 10, 3, '702', null,
    //     275, 75, 300, 300, 50, 50, 200, undefined, null, undefined, undefined, null, null, null,
    //     'B 500 SD', 5, 'C', '1w.'
    //   )
    // );

    // this.generateRandomShapes(4);
  }

  ngAfterViewChecked() {
    this.tableHeight = 300; // 25 * (this.shapes.length + 4);
  }

  shapeObject (
    valid, prefix, pcgr, gr, total, diameter, type, length,
    a, b, c, d, e, f, g, x, y, v, s, t, u, totalweight,
    steelsort, radius, rev, leadtime
  ) {
    return {
      valid: !!valid, prefix, pcgr, gr, total, diameter, type, length,
      a, b, c, d, e, f, g, x, y, v, s, t, u, totalweight,
      steelsort, radius, rev, leadtime
    };
  }

  // table event callback
  tableClick = (_, __, coords) => this.draw(
    this.shapes[coords.row]
  )

  // table event callback
  tableChange = (_, change) => {
    const shape = this.shapes[change[0][0]];
    this.validate(shape);
    this.draw(shape);
  }

  afterRender = () => {
    if (this.startTimeRendering) {
      const endTime = new Date().getTime();
      this.elapsedTimeGenerating = Math.round(endTime - this.startTimeRendering);
      this.startTimeRendering = null;
    }
  }

  tableSelect = (_, __, ___, row) => this.draw(this.shapes[row]);

  /**
   * Apply a desplacement to given scalar coordenates
   * from the polar ones (angle and longitude)
   */
  nextCoords([lastX, lastY]: number[], angle, longitude) {
    // radians to degrees
    // angle = angle * (180 / Math.PI);
    return [
      lastX + (longitude * Math.cos(angle)),
      lastY + (longitude * Math.sin(angle))
    ];
  }

  draw(shape) {
    switch (shape.type) {
      case '308': {
        this.selectedShapeCoords = this.generateCoords308(shape);
        break;
      }
      case '986': {
        this.selectedShapeCoords = this.generateCoords986(shape);
        break;
      }
      case '501': {
        this.selectedShapeCoords = this.generateCoords501(shape);
        break;
      }
      case '702': {
        this.selectedShapeCoords = this.generateCoords702(shape);
        break;
      }
      default: {
        this.selectedShapeCoords = [this.startingPoint];
      }
    }
  }

  generateCoords702(shape) {
    const coords = [this.startingPoint];
    // draw A (-90º)
    coords.push(
      this.nextCoords(
        [...coords].pop(),
        -90 * (Math.PI / 180),
        shape.a
      ));
    // B
    coords.push(this.nextCoords([...coords].pop(), 0, shape.b));
    // C
    coords.push(
      this.nextCoords(
        [...coords].pop(),
        90 * (Math.PI / 180),
        shape.c
      ));
    // D
    coords.push(this.nextCoords([...coords].pop(), 0, shape.d));
    // E
    coords.push(
      this.nextCoords(
        [...coords].pop(),
        -90 * (Math.PI / 180),
        shape.e
      ));
    // F
    coords.push(this.nextCoords([...coords].pop(), 0, shape.f));
    // G
    coords.push(
      this.nextCoords(
        [...coords].pop(),
        90 * (Math.PI / 180),
        shape.g
      ));
    return coords;
  }

  generateCoords501(shape) {
    const coords = [[
      this.startingPoint[0] * 4,
      this.startingPoint[1],
    ]];
    // A
    coords.push([
      coords[0][0] - shape.a,
      coords[0][1]
    ]);
    // draw B (-90º)
    coords.push(
      this.nextCoords(
        [...coords].pop(),
        -90 * (Math.PI / 180),
        shape.b
      ));
    // C
    coords.push(this.nextCoords([...coords].pop(), 0, shape.c));
    // D
    coords.push(
      this.nextCoords(
        [...coords].pop(),
        90 * (Math.PI / 180),
        shape.d
      ));
    // E
    coords.push(this.nextCoords([...coords].pop(), 0, -shape.e));
    return coords;
  }

  generateCoords986(shape) {
    const coords = [this.startingPoint];
    // draw first segment
    coords.push([
      coords[0][0] + shape.a,
      coords[0][1]
    ]);
    // repeated figure 4x
    for (let i = 0; i < 3; i++) {
      // draw B (-90º)
      coords.push(
        this.nextCoords(
          [...coords].pop(),
          -90 * (Math.PI / 180),
          shape.b
        ));
      // draw C (0º)
      coords.push(this.nextCoords([...coords].pop(), 0, shape.c));
      // draw B (90º)
      coords.push(
        this.nextCoords(
          [...coords].pop(),
          90 * (Math.PI / 180),
          shape.b
        ));
      // draw D
      coords.push(this.nextCoords([...coords].pop(), 0, shape.d));
    }
    return coords;
  }

  generateCoords308(shape) {
    // deduce the angle
    const V = Math.asin(shape.d / shape.b);
    const coords = [this.startingPoint];

    // draw the first segment (A)
    coords.push([
      coords[0][0] + shape.a,
      coords[0][1]
    ]);
    // draw segment B with angle traslation
    coords.push(this.nextCoords([...coords].pop(), V, shape.b));
    // draw segment C with angle in opposite direction = 0º
    coords.push(this.nextCoords([...coords].pop(), 0, shape.c));
    return coords;
  }

  validateAll() {
    const startTime = new Date().getTime();
    this.shapes.forEach(shape => {
      this.validate(shape);
    });
    const endTime = new Date().getTime();
    this.elapsedTimeValidating = Math.round(endTime - startTime);
  }

  validate(shape) {
    this.validations++;
    const max = 500;
    const min = 0;
    let valid;

    // Shape parameters "validation"
    for (const param in shape) {
      if (shape.hasOwnProperty(param)) {
        valid = (shape[param] > min) && (shape[param] < max);
      }
    }

    // Longitudes "validation"
    let long = shape.a + shape.b + shape.c + shape.d + shape.e + shape.f + shape.g;
    long = long * 2 * shape.radius;
    if (long > shape.a * 100) {
      valid = !(long > shape.a * 100) && valid;
    }

    // trigonometric measurements
    valid = this.trigonometricMeasurements(shape) && valid;
  }

  trigonometricMeasurements(shape) {
    const A = () => Math.sqrt(
      Math.pow(((shape.c / (shape.radians + shape.diameter)) - 1), 2)
      - (1 + Math.pow(((shape.b / (shape.radians + shape.diameter)) - 1), 2))
      * Math.pow(((shape.c / (shape.radians + shape.diameter)) - 1), 2)
      - Math.pow(((shape.b / (shape.radians + shape.diameter)) - 1), 2)
    );
    const B = () => (1 + Math.pow(((shape.b / (shape.radians + shape.diameter)) - 1), 2))
      - (shape.b / (shape.radians + shape.diameter)) - 1;

    Math.min(
      Math.asin(A() / B()),
      Math.asin(A() / B())
    );
    return false;
  }

  addServerFigures() {
    this.http.get('./assets/' + this.FILENAMES[this.numberToDownload] + '.json')
      .subscribe((shapes) => this.shapes = this.shapes.concat(shapes));
  }

  addGeneratedFigures(input) {
    this.generateRandomShapes(input.value);
    input.value = '';
  }

  generateRandomShapes(n) {
    this.startTimeRendering = new Date().getTime();
    const randomShapes = [];

    for (let i = 0; i < n; i++) {
      randomShapes.push(
        this.shapeObject(
          // tslint:disable-next-line:max-line-length
          Math.floor(Math.random() * 2), this.rand(100), this.rand(100), this.rand(100), this.rand(10), this.DIMAMETERS[this.rand(this.DIMAMETERS.length) - 1], this.TYPES[this.rand(this.TYPES.length) - 1], null,
          // tslint:disable-next-line:max-line-length
          this.rand(300), this.rand(300), this.rand(300), this.rand(300), this.rand(300), this.rand(300), this.rand(300), this.rand(300), this.rand(300), this.rand(300), this.rand(300), this.rand(300), this.rand(300), null,
          // tslint:disable-next-line:max-line-length
          'A 1500 SD', this.RADIUS[this.rand(this.RADIUS.length) - 1], this.REVS[this.rand(this.REVS.length) - 1], '1d.'
        )
      );
    }
    this.shapes = this.shapes.concat(randomShapes);

    const blob = new Blob([JSON.stringify(this.shapes)], {type: 'text/json'});
    const link = <HTMLAnchorElement>document.getElementById('shapesDonlowdLink');
    if (link) {
      link.download = 'shapes.json';
      link.href = window.URL.createObjectURL(blob);
    }
  }

  // random natural up to n
  rand(n) {
    return Math.floor((Math.random() * n) + 1);
  }

}
